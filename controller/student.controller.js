const { Students, Vehicles } = require("../models")
const uuid = require("uuid")

class StudentController {
  static async getStudents(req, res) {
    try {
      const options = {
        include: [
          {
            model: Vehicles,
            attributes: ["type"]
          }
        ]
      }
      const data = await Students.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getStudent(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
      include: [
        {
          model: Vehicles
        }
      ]
    }
    const data = await Students.findOne(options)

    res.status(200).json({ data })
  }

  static async updateStudent(req, res) {
    const { name, dom, isGraduate, age } = req.body

    const payload = {
      name, dom, isGraduate, age
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await Students.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createStudent(req, res) {
    const id = uuid.v4()
    const { name, dom, isGraduate, age } = req.body
    const payload = {
      name, dom, isGraduate, age, id
    }

    const newStudent = await Students.create(payload)
    res.status(200).json({ data: newStudent })
  }


  static async deleteStudent(req, res) {
    const id = req.params.id
    const deleted = await Students.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = StudentController