const StudentController = require("./student.controller")
const VehicleController = require("./vehicles.controller")
const ClassController = require("./class.controller")


module.exports = {
  StudentController,
  VehicleController,
  ClassController
}