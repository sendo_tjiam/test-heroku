const { StudentController } = require("../controller")
const route = require("express").Router()


route.get("/", StudentController.getStudents)
route.get("/:id", StudentController.getStudent)
route.post("/", StudentController.createStudent)
route.put("/:id", StudentController.updateStudent)
route.delete("/:id", StudentController.deleteStudent)


module.exports = route