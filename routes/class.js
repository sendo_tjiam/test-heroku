const { ClassController } = require("../controller")
const route = require("express").Router()


route.get("/", ClassController.getClasses)
route.get("/:id", ClassController.getClass)
route.post("/", ClassController.createClass)
route.put("/:id", ClassController.updateClass)
route.delete("/:id", ClassController.deleteClass)


module.exports = route