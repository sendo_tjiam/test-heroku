const studentRoute = require("./student")
const vehicleRoute = require("./vehicles")
const classRoute = require("./class")
const route = require("express").Router()


route.use("/student", studentRoute)
route.use("/vehicle", vehicleRoute)
route.use("/class", classRoute)


module.exports = route