'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Students extends Model {
    static associate(models) {
      Students.hasMany(models.Vehicles)
      Students.belongsToMany(models.Class, { through: models.StudentClass })
      // define association here
    }
  };
  Students.init({
    id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    dom: DataTypes.STRING,
    age: DataTypes.NUMBER,
    isGraduate: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Students',
  });
  return Students;
};