'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Class extends Model {

    static associate(models) {
      Class.belongsToMany(models.Students, { through: models.StudentClass })
    }
  };
  Class.init({
    Name: DataTypes.STRING,
    Day: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Class',
  });
  return Class;
};