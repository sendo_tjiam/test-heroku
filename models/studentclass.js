'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentClass extends Model {

    static associate(models) {
      models.Students.belongsToMany(models.Class, { through: models.StudentClass })
      models.Class.belongsToMany(models.Students, { through: models.StudentClass })

    }
  };
  StudentClass.init({
    StudentId: DataTypes.STRING,
    ClassId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'StudentClass',
  });
  return StudentClass;
};